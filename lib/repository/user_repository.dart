import 'package:firebase_database/firebase_database.dart';
import 'dart:async';

class UserRepository {

  final DatabaseReference userReference = FirebaseDatabase.instance.reference().child('user');

  Future<bool> isExistingUser(String mobileNumber) async{
    Query queryRef = userReference.orderByChild('mobileNumber')
                      .equalTo(mobileNumber);
                      print(queryRef);
    DataSnapshot snapshot = await queryRef.once();
    print(snapshot.value);
    return snapshot.value != null;
  }

  bool addNewUser(String userName, String mobileNumber) {
    userReference.push().set({
      'userName' : userName,
      'mobileNumber': mobileNumber,
    });
    return true;
  }

  bool addOrupdateUser(String userName, String mobileNumber) {
    
    userReference.child(mobileNumber).update(
      {
      'userName' : userName,
      'mobileNumber': mobileNumber,
      }
    );
    return true;
  }
}

