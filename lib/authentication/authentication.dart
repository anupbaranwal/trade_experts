import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';

abstract class BaseAuth {
  Future<String> signWithEmailAndPassword(String email, String password);
  Future<String> createUserWithEmailAndPassword(String email, String password);
  Future<String> signInWithPhoneNumber(String verificationId, String smsCode);
  Future<void> verifyPhoneNumber(
     String phoneNumber,
     Duration timeout,
     int forceResendingToken,
     PhoneVerificationCompleted verificationCompleted,
     PhoneVerificationFailed verificationFailed,
     PhoneCodeSent codeSent,
     PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout);
  Future<String> currentUser();
}

class Auth implements BaseAuth {
  FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  @override
  Future<String> createUserWithEmailAndPassword(String email, String password) async{
    FirebaseUser user = await _firebaseAuth.createUserWithEmailAndPassword(email: email, password: password);
    return user.uid;
  }

  @override
  Future<String> signWithEmailAndPassword(String email, String password) async{
    FirebaseUser user = await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
    return user.uid;
  }

  @override
  Future<String> signInWithPhoneNumber(String verificationId, String smsCode) async{
    FirebaseUser user;
    return user.uid;
  }

  @override
  Future<void> verifyPhoneNumber(
     String phoneNumber,
     Duration timeout,
     int forceResendingToken,
     PhoneVerificationCompleted verificationCompleted,
     PhoneVerificationFailed verificationFailed,
     PhoneCodeSent codeSent,
     PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout) async {
       await _firebaseAuth.verifyPhoneNumber(
        phoneNumber: phoneNumber,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout,
        codeSent: codeSent,
        timeout: timeout,
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed);
  }

  @override
  Future<String> currentUser() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    if(user == null) {
      return null;
    }
    return user.uid;
  }

  


}