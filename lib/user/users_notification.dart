import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:trade_experts/GradientAppBar.dart';

class UsersNotification extends StatefulWidget {
  @override
  _UsersNotificationState createState() => _UsersNotificationState();
}

class _UsersNotificationState extends State<UsersNotification> {
  List<String> _notifications = List<String>();
  final referenceNotification = FirebaseDatabase.instance.reference().child('notification');
  void getNotification() {
    referenceNotification.once().then((DataSnapshot notificationSnapShot){
      print(notificationSnapShot.value.values);
      this.setState(() {
        for(var notification in notificationSnapShot.value.values) {
          _notifications.add(notification['notificationText']);
        }
      });
    });
  }

  @override
  void initState() {
    this.getNotification();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        // constraints: new BoxConstraints.expand(),
        child: Column(
          children: <Widget>[
            GradientAppBar('Trade Experts', false),
            Expanded(
              child: ListView.builder(
                padding: EdgeInsets.symmetric(vertical: 2.0),
                itemCount: _notifications.length,
                itemBuilder: (BuildContext context, int index) {
                  return NotificationRow(notification: _notifications[index]);
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}

class NotificationRow extends StatelessWidget {
NotificationRow({
    Key key,
    @required String notification
  }): _notification = notification, super(key: key);
  final String _notification;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.teal[100],
      margin: EdgeInsets.only(left: 10, bottom: 2.0, right: 10, top: 8),
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: Text(_notification, style: TextStyle(color: Colors.black, fontFamily: 'Poppins', fontWeight: FontWeight.w600),)
      ),
    );
  }
}

