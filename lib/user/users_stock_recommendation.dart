import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:trade_experts/GradientAppBar.dart';
import 'package:trade_experts/entity/Stock.dart';

class UserStockRecommendation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body: Container(
        child: Column(
          children: <Widget>[
            GradientAppBar("Trade Experts", true),
            Expanded(
              // child:  AdminStockRecommendation()
              child:  Recommendation()
              // child: AdminNotification(),
              // child: UsersNotification()
            )
          ],
        ),
      ),
    );
  }
}

class Recommendation extends StatefulWidget {
  @override
  _RecommendationState createState() =>
      _RecommendationState();
}

class _RecommendationState extends State<Recommendation> {
  List<Stock> _stocks =  List<Stock>();
  final reference = FirebaseDatabase.instance.reference();

  void getStocks() async {
    await reference.child('stocks').once().then((DataSnapshot stocksSnap) {
      this.setState(() {
        print(stocksSnap.value.values);
        for (var stock in stocksSnap.value.values) {
          _stocks.add(Stock.fromJson(stock));
        }
      });
    });
  }

  @override
  void initState() {
    getStocks();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: _stocks.length,
        itemBuilder: (BuildContext context, int index) {
          return StockRow(stock: _stocks[index]);
        },
        padding: EdgeInsets.symmetric(vertical: 4.0)
    );
  }
}

class StockRow extends StatelessWidget {
  StockRow({Key key, @required Stock stock})
      : _stock = stock,
        super(key: key);
  final Stock _stock;

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 180.0,
        margin: EdgeInsets.symmetric(vertical: 4.0, horizontal:15.0),
        child:  Container(
          margin:  EdgeInsets.all(1.5),
          decoration:  BoxDecoration(
            // color:  Color(0xFF333366),
            shape: BoxShape.rectangle,
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Color(0xFF331066),Color(0xFF331045)],
            ),
            boxShadow: <BoxShadow>[
                 BoxShadow(  
                color: Colors.black87,
                blurRadius: 6.0,
                offset:  Offset(0.0, 9.0),
              ),
            ],
            borderRadius:  BorderRadius.circular(8.0)
          ),
          child: CardContent(stock: _stock),
        ));
  }
}

class CardContent extends StatelessWidget {
  const CardContent({Key key, @required Stock stock})
      : _stock = stock,
        super(key: key);
  final Stock _stock;

  static final baseTextStyle = const TextStyle(fontFamily: 'Poppins');
  static final headerTextStyle = baseTextStyle.copyWith(
      color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.w600);

  static final regularTextStyle = baseTextStyle.copyWith(
      color: const Color(0xffb6b2df),
      fontSize: 8.0,
      fontWeight: FontWeight.w400);
  static final subHeaderTextStyle = regularTextStyle.copyWith(fontSize: 10.0);

  @override
  Widget build(BuildContext context) {
    return  Container(
        margin:  EdgeInsets.fromLTRB(10.0, 6.0, 10.0, 6.0),
        constraints:  BoxConstraints.expand(),
        child:  Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                 Text(
                  _stock.stockName,
                  style: headerTextStyle,
                ),
                 Text(
                  _stock.recommendation,
                  style: headerTextStyle,
                ),
              ]
            ),
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                   Text(_stock.stockCode, style: subHeaderTextStyle),
                   Text(
                    '${_stock.price}',
                    style: headerTextStyle,
                  ),
                ]),
             Container(
                margin:  EdgeInsets.symmetric(vertical: 5.0),
                height: 2.0,
                color:  Color(0xff00c6ff)),
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                   Text(
                    'TARGET',
                    style: headerTextStyle,
                  ),
                   Text(
                    'STOPLOSS',
                    style: headerTextStyle,
                  ),
                ]),
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                   Text(
                    '${_stock.target}',
                    style: headerTextStyle,
                  ),
                   Text(
                    '${_stock.stoploss}',
                    style: headerTextStyle,
                  ),
                ]),
            Divider(
              color: Colors.white,
              height: 8.0,
            ),
            Text(
              '* We are not sebi certified advisor. For actual trading consult a recognized financial advisor. We are not resposible for any loss incurred due to our training calls mentioned here',
              style: TextStyle(fontSize: 9.0, color: Colors.white),
            ),
            Container(
              alignment: Alignment.centerRight,
              child: Text(
                DateTime.now().day.toString() +'-'+ DateTime.now().month.toString() +'-'+ DateTime.now().year.toString(),
                style: subHeaderTextStyle
              ),
            )
          ],
        ));
  }
}
