import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:trade_experts/authentication/authentication.dart';
import 'package:trade_experts/landing_page.dart';
import 'package:trade_experts/user/users_stock_recommendation.dart';
import 'dart:async';

class RoutPage extends StatefulWidget {
  RoutPage({this.auth});
  final BaseAuth auth;

  @override
  _RoutPageState createState() => _RoutPageState();
}

enum AuthStatus {
  notSignedIn,
  signedIn
}

class _RoutPageState extends State<RoutPage> {

  AuthStatus authStatus = AuthStatus.notSignedIn;


  void onSignedIn() {
    print("onSignedIn called");
    setState(() {
      authStatus = AuthStatus.signedIn;
    });
  }

  Future<bool> getLoginInformation() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool('isUserSignedIn') ?? false;
  }

  @override
  void initState() {
    super.initState();
    getLoginInformation().then((isUserSignedIn) {
      setState(() {
        authStatus = isUserSignedIn ? AuthStatus.signedIn: AuthStatus.notSignedIn;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    switch (authStatus) {
      case AuthStatus.notSignedIn:
        return LoginPage(
          authentication: widget.auth,
          onSignedIn: onSignedIn
        );
      case AuthStatus.signedIn:
        return UserStockRecommendation();
      default:
        return LoginPage(
          authentication: widget.auth,
          onSignedIn: onSignedIn
        );
    }
  }
}