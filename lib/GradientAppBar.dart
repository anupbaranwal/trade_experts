import 'dart:math';

import 'package:flutter/material.dart';
import 'package:trade_experts/ChatScreen.dart';
import 'package:trade_experts/user/users_notification.dart';

class GradientAppBar extends StatelessWidget {

  final String title;
  final bool isMainPage;
  final double barHeight = 62.0;

  GradientAppBar(this.title, this.isMainPage);

  @override
  Widget build(BuildContext context) {
    final double statusBarHeight = MediaQuery
      .of(context)
      .padding
      .top;

    return  Container(
      decoration:  BoxDecoration(
        // borderRadius: BorderRadius.only(
        //   bottomLeft: Radius.circular(10),
        //   bottomRight: Radius.circular(10)
        // ),
        // color: Color(0xFF009688),
        gradient:  LinearGradient(
          // colors: [Color(0xFF009448),Color(0xFF009688)],
          colors: [Color(0xFF016b61),Color(0xFF04ccb9)],
          // begin: const FractionalOffset(0.0, 0.0),
          // end: const FractionalOffset(0.5, 0.0),
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          // stops: [0.0, 0.5],
          // tileMode: TileMode.clamp
        ),
      ),
      padding:  EdgeInsets.only(top: statusBarHeight),
      height: statusBarHeight + barHeight,
      child: this.isMainPage? _mainPageToolBar(context) : _getSecondryPageToolBar(context)
    );
  }

  Container _getSecondryPageToolBar(BuildContext context) {
    return Container(
      margin:  EdgeInsets.only(left: 10.0),
      constraints:  BoxConstraints.expand(),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          BackButton(color: Colors.white),
          Expanded(
            child:  Text(
              title,
              style: const TextStyle(
                color: Colors.white,
                fontFamily: 'Poppins',
                fontWeight: FontWeight.w600,
                fontSize: 20.0
              )
            ),
          )
        ],
      )
    );
  }

  Container _mainPageToolBar(BuildContext context) {
    return Container(
      margin:  EdgeInsets.only(left: 10.0),
      constraints:  BoxConstraints.expand(),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child:  Text(
              title,
              style: const TextStyle(
                color: Colors.white,
                fontFamily: 'Poppins',
                fontWeight: FontWeight.w600,
                fontSize: 20.0
              )
            ),
          ),
          IconButton(
            icon:  Icon(Icons.forum), 
            onPressed: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ChatScreen()),
              );
            },
            color: Colors.white,
            iconSize: 32.0,
          ),
          Stack(
            children: <Widget>[
              IconButton(
                icon:  Icon(Icons.notifications_active),
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => UsersNotification()),
                  );
                },
                color: Colors.white,
                iconSize: 32.0,
              ),
              CircleAvatar(
                backgroundColor: Colors.orange[100],
                child: Text(
                   (Random().nextInt(8)+1).toString()
                ),
                radius: 12.0,
              )
            ],
          )
        ],
      )
    );
  }
}