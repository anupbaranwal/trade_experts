import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:trade_experts/authentication/authentication.dart';
import 'package:trade_experts/repository/user_repository.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:async';

final analytics = new FirebaseAnalytics();

enum FormType {
  login,
  register
}

class UserForm extends StatefulWidget {

  UserForm({this.authentication, this.onSignedIn});
  final BaseAuth authentication;
  final VoidCallback onSignedIn;

  @override
  _UserFormState createState() => _UserFormState();
}

class _UserFormState extends State<UserForm> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final UserRepository _userRepository = UserRepository();
  String _phoneNumber, _name;
  String smsCode;
  String verificationId;

  Future<void> verifyPhone() async {
    final PhoneCodeAutoRetrievalTimeout autoRetrieve = (String verId) {
      this.verificationId = verId;
    };

    final PhoneCodeSent smsCodeSent = (String verId, [int forceCodeResend]) {
      this.verificationId = verId;
      smsCodeDialog(context).then((value) {
        print('Signin in progress $value');
      });
    };

    final PhoneVerificationCompleted verifiedSuccess = (FirebaseUser user) {
      showToast("verification success");
      print('verified');
    };

    final PhoneVerificationFailed veriFailed = (AuthException exception) {
      showToast("verification failed ${exception.message}");
      print('${exception.message}');
    };

    await FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: this._phoneNumber,
      codeAutoRetrievalTimeout: autoRetrieve,
      codeSent: smsCodeSent,
      timeout: const Duration(seconds: 30),
      verificationCompleted: verifiedSuccess,
      verificationFailed: veriFailed
    );
  }

  

  Future<bool> smsCodeDialog(BuildContext context) async {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
          title: Text('Enter sms Code'),
          content: TextField(
            onChanged: (value) {
              this.smsCode = value;
            },
          ),
          contentPadding: EdgeInsets.all(10.0),
          actions: <Widget>[
            new FlatButton(
              child: Text('Done'),
              onPressed: () {
                showToast("before getting current user");
                signIn();
              },
            )
          ],
        );
      }
    );
  }

showToast(message) {
  Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0
    );
}

  signIn() {
    // var signInCredential = PhoneAuthProvider.getCredential(verificationId: verificationId, smsCode: smsCode);
    // FirebaseAuth.instance.signInWithCredential(signInCredential).then((user){
    //   showToast("after verifying current user");
    //     Navigator.of(context, rootNavigator: true).pop();
    //   _userRepository.addOrupdateUser(_name, _phoneNumber);
    //   // saveLoginInformation();
    //   showToast("After updating user");
    //   Navigator.of(context).pushReplacementNamed('/userStockRecommendation');
    // }).catchError((onError) {
    //   showToast("Failed signin"+ onError); 
    // });


      FirebaseAuth.instance.signInWithPhoneNumber(verificationId: verificationId, smsCode: smsCode)
      .then((user) {
        showToast("after verifying current user");
        Navigator.of(context, rootNavigator: true).pop();
      _userRepository.addOrupdateUser(_name, _phoneNumber);
      // saveLoginInformation();
      showToast("After updating user");
      Navigator.of(context).pushReplacementNamed('/userStockRecommendation');
    }).catchError((e) {
      print(e);
    });
  }

  bool _isFormValid() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      //  _userRepository.addNewUser('Anup', 'abc@12387', '+919620444966');
      return true;
    }
    return false;
  }

  Future<bool> isAuthorized() async {
    // bool isAuth = await _userRepository.isAuthorizedUser(_phoneNumber, _password);
    return true;
  }

  void saveLoginInformation() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('isUserSignedIn', true);
  }

  void _submitForm() async {
    if(_isFormValid()) {
      print("checking");
      verifyPhone();
    }
  }

  String _phoneNumberValidator(String value) {
    if (value.length != 10)
      return 'Mobile Number must be of 10 digit';
    else
      return null;
  }

  String _nameValidator(String name) {
    if(name.length < 3)
      return 'Name must be more than 2 charater';
    else
      return null;
  }

  List<Widget> getFormContent(BuildContext context) {
    return [
      Container(
            child: Image(
              width: 80,
              image: AssetImage('assets/images/target_logo.png'),
            ),
          ),
           SizedBox(height: 15.0),
      Container(
        width: MediaQuery.of(context).size.width/1.15,
        child: TextFormField(
            keyboardType: TextInputType.text,
            validator: _nameValidator,
            onSaved: (value) => this._name = value,
            decoration: const InputDecoration(
              prefixIcon: Icon(Icons.person_outline, color: Colors.deepOrange),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
              focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.deepOrange)),
              labelText: 'Name',
              labelStyle: TextStyle(
                fontFamily: 'Poppins',
                fontWeight: FontWeight.w500,
                color: Color(0xFF009688)
              )
            ),
          ),
      ),
      SizedBox(height: 15),
      Container(
        width: MediaQuery.of(context).size.width/1.15,
        child: TextFormField(
            keyboardType: TextInputType.phone,
            validator: _phoneNumberValidator,
            onSaved: (value) => this._phoneNumber = '+91'+value,
            decoration: const InputDecoration(
              prefixIcon: Icon(Icons.phone_iphone, color: Colors.deepOrange),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
              focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.deepOrange)),
              labelText: 'Mobile',
              prefixText: '+91',
              labelStyle: TextStyle(
                fontFamily: 'Poppins',
                fontWeight: FontWeight.w500,
                color: Color(0xFF009688)
              )
            ),
          ),
      ),
      SizedBox(height: 15),
      Container(
        width: MediaQuery.of(context).size.width/1.5,
        child: FlatButton(
          padding: EdgeInsets.all(12),
          onPressed: _submitForm,
          color: Color(0xFF009688),
          splashColor: Colors.green,
          textColor: Colors.white,
          textTheme: ButtonTextTheme.primary,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10)
          ),
          child: Text('SIGN IN', style: TextStyle(fontFamily: 'Poppins', fontSize: 18),),
        ),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 7),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height*1.4/2.4,
      child: Form(
        key: _formKey,
        child: Column(
          children: getFormContent(context)
        ),
      ),
    );
  }
}