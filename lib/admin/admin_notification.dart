import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

class AdminNotification extends StatefulWidget {
  @override
  _AdminNotificationState createState() => _AdminNotificationState();
}

class _AdminNotificationState extends State<AdminNotification> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final reference = FirebaseDatabase.instance.reference();
  String _notification;

  void notify() {
    if (this._formKey.currentState.validate()) {
      _formKey.currentState.save(); // Save our form now.
      reference.child('notification').push().set({
        'notificationText': this._notification,
      });
      this._formKey.currentState.reset();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            TextFormField(
              keyboardType: TextInputType.multiline,
              maxLength: 300,
              maxLines: 6,
              decoration: InputDecoration(
                labelText: 'Enter Notification'
              ),
              onSaved: (String value) {
                this._notification = value;
              }
            ),
            Center(
              child: new Container(
                margin: new EdgeInsets.only(top: 20.0),
                decoration: new BoxDecoration(
                  color: Colors.blueAccent,
                  border: new Border.all(color: Colors.white, width: 2.0),
                  borderRadius: new BorderRadius.circular(10.0),
                ),
                child: new RaisedButton(
                  splashColor: Colors.orange,
                  child: new Text(
                    'Notify',
                    style: new TextStyle(
                      color: Colors.white,
                      fontSize: 20.0
                    ),
                  ),
                  onPressed: this.notify,
                  
                  color: Colors.blueAccent,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}