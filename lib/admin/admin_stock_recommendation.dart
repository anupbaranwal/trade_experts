import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:trade_experts/entity/Stock.dart';

class AdminStockRecommendation extends StatefulWidget {
  @override
  _AdminStockRecommendationState createState() => _AdminStockRecommendationState();
}


class _AdminStockRecommendationState extends State<AdminStockRecommendation> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final reference = FirebaseDatabase.instance.reference();
  final Stock stock = new Stock();
  bool _buyStock = true;
  void submit() {
    // First validate form.
    if (this._formKey.currentState.validate()) {
      _formKey.currentState.save(); // Save our form now.
      reference.child('stocks').push().set({
        'stockName': stock.stockName,
        'stockCode': stock.stockCode,
        'recommendation': stock.recommendation,
        'price': stock.price,
        'target': stock.target,
        'stoploss': stock.stoploss
      });
      this._formKey.currentState.reset();
      print('Stock data saved.');
    }
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: _formKey,
        child: ListView(
          children: <Widget>[
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                labelText: 'Stock Name'
              ),
              onSaved: (String value) {
                this.stock.stockName = value;
              }
            ),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                labelText: 'Stock Code'
              ),
              onSaved: (String value) {
                this.stock.stockCode = value;
              }
            ),
            Container(
              alignment: Alignment.centerLeft,
              child: Row(
                children: <Widget>[
                  Text('Buy?', style: TextStyle(fontSize: 30.0),),
                  new Switch(
                  value: _buyStock,
                  onChanged: (bool value) {
                    setState(() {
                      _buyStock = value;
                      this.stock.recommendation = value ? 'BUY' : 'SELL';
                    });
                  }
                ),
                ],
              ),
            ),
            TextFormField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                labelText: 'Price'
              ),
              onSaved: (String value) {
                this.stock.price = double.parse(value);
              }
            ),
            TextFormField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                labelText: 'Target'
              ),
              onSaved: (String value) {
                this.stock.target = double.parse(value);
              }
            ),
            TextFormField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                labelText: 'Stoploss'
              ),
              onSaved: (String value) {
                this.stock.stoploss = double.parse(value);
              }
            ),
            Center(
              child: new Container(
                margin: new EdgeInsets.only(top: 20.0),
                decoration: new BoxDecoration(
                  color: Colors.blueAccent,
                  border: new Border.all(color: Colors.white, width: 2.0),
                  borderRadius: new BorderRadius.circular(10.0),
                ),
                child: new RaisedButton(
                  splashColor: Colors.orange,
                  child: new Text(
                    'Recommend',
                    style: new TextStyle(
                      color: Colors.white,
                      fontSize: 20.0
                    ),
                  ),
                  onPressed: this.submit,
                  color: Colors.blueAccent,
                ),
              ),
            )
          ],
        ),
      ),
      
    );
  }
}