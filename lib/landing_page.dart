import 'package:flutter/material.dart';
import 'package:trade_experts/authentication/authentication.dart';
import 'package:trade_experts/user_form.dart';

class LoginPage extends StatefulWidget {
  LoginPage({this.authentication, this.onSignedIn});
  final BaseAuth authentication;
  final VoidCallback onSignedIn;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomPadding: true,
      body:  Stack(
        children: <Widget>[
          Image(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.cover,
            image: AssetImage('assets/images/background.png'),
          ),
          SingleChildScrollView(
            child: Column(
              children: <Widget>[
                loginHeader(context),
                loginBody()
              ],
            ),
        ),
        ]
      ),
     );
  }

  UserForm loginBody() {
    return UserForm(
      authentication: widget.authentication,
      onSignedIn: widget.onSignedIn
    );
  }
  
  Container loginHeader(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height/2.9,
      // decoration: BoxDecoration(
      //   gradient: LinearGradient(
      //     begin: Alignment.topLeft,
      //     end: Alignment.bottomRight,
      //     colors: [Color(0xFF016b61),Color(0xFF04ccb9)],
      //   ),
      //   // borderRadius: BorderRadius.only(
      //   //   bottomLeft: Radius.circular(80),
      //   // )
      // ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Text('Welcome to', style: 
            TextStyle(color: Colors.white, fontSize: 25, fontWeight: FontWeight.w100, fontFamily: 'Poppins')),
          ),
           SizedBox(height: 25.0),
          Container(
            child: Image(
              width: 250,
              image: AssetImage('assets/images/logo.png'),
            ),
          ),
        ],
      ),
    );
  }
}
