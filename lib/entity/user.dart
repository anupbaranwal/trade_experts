class User {
  String userName;
  String mobileNumber;

  User.fromJson(var value) {
    this.userName = value['userName'];
    this.mobileNumber = value['mobileNumber'];
  }
}