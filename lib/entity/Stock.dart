class Stock {
  String stockName;
  String stockCode;
  String recommendation;
  double price;
  double stoploss;
  double target;

  Stock();
  Stock.namedConst(this.stockName, this.stockCode, this.recommendation, this.price, this.stoploss, this.target);

  Stock.fromJson(var value) {
    this.stockCode = value['stockCode'];
    this.stockName = value['stockName'];
    this.stoploss = value['stoploss'].toDouble();
    this.price = value['price'].toDouble();
    this.target = value['target'].toDouble();
    this.recommendation = value['recommendation'];
  }
}
