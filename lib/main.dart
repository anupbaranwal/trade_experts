import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:trade_experts/authentication/authentication.dart';
import 'package:trade_experts/router.dart';
import 'package:trade_experts/user/users_stock_recommendation.dart';
import 'package:trade_experts/util/Themes.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final BaseAuth auth = Auth();
  ThemeData buildTheme() {
    final ThemeData base = ThemeData();
    return base.copyWith(
      hintColor: Color(0xFF009688),
      inputDecorationTheme: InputDecorationTheme(
        labelStyle: TextStyle(
          color: Colors.white,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      routes: <String, WidgetBuilder>{
        '/userStockRecommendation': (context) => UserStockRecommendation()
      },
      debugShowCheckedModeBanner: false,
      theme: defaultTargetPlatform == TargetPlatform.iOS
          ? Themes.kIOSTheme
          : buildTheme(),
      // home: new HomePage()
      home: RoutPage(auth: auth)
    );
  }
}